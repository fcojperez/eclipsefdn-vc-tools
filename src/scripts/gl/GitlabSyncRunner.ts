/* eslint-disable comma-dangle */
/** ***************************************************************
 Copyright (C) 2022 Eclipse Foundation, Inc.

 This program and the accompanying materials are made
 available under the terms of the Eclipse Public License 2.0
 which is available at https://www.eclipse.org/legal/epl-2.0/

  Contributors:
    Martin Lowe <martin.lowe@eclipse-foundation.org>

 SPDX-License-Identifier: EPL-2.0
******************************************************************/

import {
  AccessLevel, GroupSchema, IssueSchema, MemberSchema, ProjectExtendedSchema, ProjectSchema, UserSchema
} from '@gitbeaker/core/dist/types/types';
import axios from 'axios';
import parse from 'parse-link-header';
import { v4 } from 'uuid';
import { Logger } from 'winston';
import { EclipseAPI, EclipseApiConfig } from '../../eclipse/EclipseAPI';
import { getLogger } from '../../helpers/logger';
import { getBaseConfig, SecretReader } from '../../helpers/SecretReader';
import { EclipseProject, InterestGroup } from '../../interfaces/EclipseApi';
import { GitlabWrapper } from './GitlabWrapper';

const adminPermissionsLevel = 50;
const maintainerPermissionsLevel = 40;
const allowlistedUsers: string[] = ['webmaster', 'root'];

/**
 * Used for private repo expiry
 */
const repoExpiryDays = 90;
const hoursInDay = 24;
const minsInHour = 60;
const millisInMinute = 60000;
const repoExpiryAsMillis = repoExpiryDays * hoursInDay * minsInHour * millisInMinute;

/**
 * Represents the nested group cache that can represent the relationships between groups and to simplify child lookups.
 */
interface GroupCache {
  _self: GroupSchema | null;
  projectTargets: string[];
  children: Record<string, GroupCache>;
}

export interface EclipseUserAccess {
  url: string;
  accessLevel: AccessLevel;
}

export interface GitlabSyncRunnerConfig {
  host: string;
  provider: string;
  secretLocation?: string;
  project?: string;
  verbose: boolean;
  devMode: boolean;
  dryRun: boolean;
  user: string,
  rootGroup?: string;
  staging?: boolean;
}

export class GitlabSyncRunner {
  // internal state
  accessToken = '';
  eclipseToken = '';
  config: GitlabSyncRunnerConfig;
  logger: Logger;

  // api access
  gitlabWrapper: GitlabWrapper;
  eApi: EclipseAPI;

  // caches to optimize calling
  nestedGroupCache: GroupCache = {
    _self: null,
    children: {},
    projectTargets: [],
  };
  eclipseProjectCache: Record<string, EclipseProject> = {};
  botsCache: Record<string, string[]> = {};

  /**
   * Sets the internal config with a few default values and creates the bindings for the APIs that are
   * accessed during the run of this script.
   *
   * @param config the initial script configuration object.
   */
  constructor(config: GitlabSyncRunnerConfig) {
    this.config = Object.assign(
      {
        host: 'http://gitlab.eclipse.org/',
        provider: 'oauth2_generic',
        verbose: false,
        devMode: false,
        dryRun: false,
        user: 'webmaster',
        rootGroup: 'eclipse',
      },
      config
    );

    this.logger = getLogger(this.config.verbose ? 'debug' : 'info', 'main');
    this._prepareSecret();

    // create API instances
    this.gitlabWrapper = new GitlabWrapper({
      host: this.config.host,
      token: this.accessToken,
      verbose: this.config.verbose,
      user: this.config.user,
      rootGroup: this.config.rootGroup,
    });
    const eclipseAPIConfig: EclipseApiConfig = JSON.parse(this.eclipseToken);
    eclipseAPIConfig.testMode = this.config.devMode;
    eclipseAPIConfig.verbose = this.config.verbose;
    this.eApi = new EclipseAPI(eclipseAPIConfig);
  }

  /**
   * Prepares the secrets required for the script to run. Specifically the eclipseToken used for Eclipse
   * API access and the accessToken which is used for sudo+api access on the Gitlab instance targeted by
   * this script.
   */
  _prepareSecret() {
    // retrieve the secret API file root if set
    const settings = getBaseConfig();
    if (this.config.secretLocation !== undefined) {
      settings.root = this.config.secretLocation;
    }
    const reader = new SecretReader(settings);
    let data = reader.readSecret('access-token');
    if (data !== null) {
      this.accessToken = data.trim();
      // retrieve the Eclipse API token (needed for emails)
      data = reader.readSecret('eclipse-oauth-config');
      if (data !== null) {
        this.eclipseToken = data.trim();
      } else {
        this.logger.error('Could not find the Eclipse OAuth config, exiting');
        process.exit(1);
      }
    } else {
      this.logger.error('Could not find the GitLab access token, exiting');
      process.exit(1);
    }
  }

  /**
   * Run the full sync script, syncing the PMI to the Gitlab instance targeted by the script. This script
   * will sync the namespace groups named in projects with the users that are set as members of the project.
   * It will also clear users added outside of this process with the exception of bot users to maintain more
   * strict control of the access permissions.
   *
   * @returns a promise that is completed once the run completes.
   */
  async run(): Promise<void> {
    // prepopulate caches to optimally retrieve info used in sync ops
    await this.gitlabWrapper.prepareCaches();
    await this.prepareCaches();
    // fetch org group from results, create if missing
    this.logger.info('Starting sync');
    const g = this.getRootGroup();
    if (g._self === null) {
      this.logger.error(`Unable to start sync of GitLab content. Base group (${this.config.rootGroup}) could not be found`);
      return;
    }
    // perform base project sync
    await this.performProjectSync(this.eclipseProjectCache);
    // perform IG syncing
    await this.performIGSync();

    // perform cleanup operations to clean out extra users and flag private repos fro deletion
    this.cleanupGroups();
    this.cleanupProjects();
    this.managePrivateProjects();
  }

  async performProjectSync(cache: Record<string, EclipseProject>, rootGroup: string = this.config.rootGroup) {
    for (const projectIdx in cache) {
      const project = cache[projectIdx];
      if (this.config.project !== undefined && project.short_project_id !== this.config.project) {
        this.logger.info(`Project target set ('${this.config.project}'). Skipping non-matching project ID ${project.short_project_id}`);
        continue;
      }
      this.logger.info(`Processing ${project.project_id}`);

      // fetch group namespace indicated by the project and ensure format
      const actualNamespace = project.gitlab.project_group;
      const [projectNamespace, projectNamespaceTLP] = [
        `${rootGroup}/${project.short_project_id}`,
        `${rootGroup}/${project.top_level_project}/${project.short_project_id}`,
      ];
      if (actualNamespace === undefined || actualNamespace.trim() === '') {
        this.logger.info(`Skipping project '${project.project_id}' as it has no Gitlab namespace`);
        continue;
      } else if (
        actualNamespace.localeCompare(projectNamespace, undefined, { sensitivity: 'base' }) !== 0 &&
        actualNamespace.localeCompare(projectNamespaceTLP, undefined, { sensitivity: 'base' }) !== 0
      ) {
        this.logger.info(
          `Skipping namespace '${actualNamespace}' for project '${project.short_project_id}', does not match allowed formats`
        );
        continue;
      }
      this.logger.info(`Processing '${project.short_project_id}'`);

      // check group cache to ensure well formed.
      const namespaceGroup = await this.getGroup(actualNamespace, rootGroup);
      if (namespaceGroup === null || namespaceGroup._self === null) {
        this.logger.error(`Could not find group with namespace ${actualNamespace}`);
        continue;
      }
      // get the list of users to be added for current project
      const userList = this.getUserList(project);
      // for each user, get their gitlab user and add to the project group
      const usernames = Object.keys(userList);
      // update the group to add the users for the current project
      for (const usernameIdx in usernames) {
        const uname = usernames[usernameIdx];
        const user = await this.getUser(uname, userList[uname].url);
        if (user === null) {
          this.logger.verbose(`Could not retrieve user for UID '${uname}', skipping`);
          continue;
        }

        await this.addUserToGroup(user, namespaceGroup._self, userList[uname].accessLevel);
        // if not tracked, track current project for group for post-sync cleanup
        if (namespaceGroup.projectTargets.indexOf(project.project_id) === -1) {
          namespaceGroup.projectTargets.push(project.project_id);
        }
      }

      // retrieve bots for current project and add them to the groups
      for (const botIdx in this.botsCache[project.project_id]) {
        const bot = this.botsCache[project.project_id][botIdx];
        this.logger.verbose(`Found ${bot} for ${project.project_id}`);
        // get the bot user if it exists already
        const botUser = await this.getUser(bot, bot);
        if (botUser == null) {
          this.logger.info(
            `Could not retrieve user for bot user ${bot} for project ${project.project_id}, ` + 'not attempting to add to group'
          );
          continue;
        }
        // add bot user to the group
        this.logger.verbose(`Adding bot ${bot} to group ${namespaceGroup._self.path}`);
        await this.addUserToGroup(botUser, namespaceGroup._self, maintainerPermissionsLevel);
      }
    }
  }

  async performIGSync() {
    this.logger.debug('performIGSync()');
    const interestGroups = await this.eApi.interestGroups();
    this.logger.info(`Converting ${interestGroups.length} interest groups into projects for sync.`);
    const adaptedInterestGroups = this.adaptInterestGroupsToProject(interestGroups);
    this.logger.info(`Adapted interest groups into ${adaptedInterestGroups.length} active project stubs after applied filters`);
    this.logger.info(`Begining sync of ${adaptedInterestGroups.length} interest groups into 'eclipse-ig' group.`);
    const mapping = adaptedInterestGroups
      .filter(ep => ep.project_id.length > 0)
      .reduce((acc, item) => ({ ...acc, [item.project_id]: item }), {} as Record<string, EclipseProject>);
    if (Object.keys(mapping).length > 0) {
      await this.performProjectSync(mapping, 'eclipse-ig');
    }
  }

  /**
   * Generates the caches needed for running the Gitlab sync process.
   */
  async prepareCaches() {
    // get raw project data and post process to add additional context
    try {
      this.logger.info('Populating projects cache');
      const data = await this.eApi.eclipseAPI();

      this.logger.info('Loading Eclipse bots');
      // get the bots for the projects
      this.logger.info('Populating bots cache');
      const rawBots = await this.eApi.eclipseBots();
      this.botsCache = this.eApi.processBots(rawBots, 'gitlab.eclipse.org');

      this.logger.info('Populating Gitlab groups cache');
      const groups = this.gitlabWrapper.getAllGroups();

      this.logger.info('Processing Gitlab groups for cache');
      // generates the nested cache
      this.generateGroupsCache(groups);

      this.logger.info('Mapping Gitlab projects');
      this.eclipseProjectCache = data.reduce((acc, item) => ({ ...acc, [item.project_id]: item }), {} as Record<string, EclipseProject>);
    } catch (e) {
      this.logger.error(e);
      this.logger.error(`Cannot fetch resources associated with sync operations, exiting: ${e}`);
      process.exit(1);
    }
  }

  /**
   * Iterate through each group, checking self and ancestor project users and comparing against the current groups users to ensure that
   * there are no additional users added with permissions.
   */
  async cleanupGroups(currentLevel: GroupCache = this.getRootGroup(), collectedProjects: string[] = []) {
    this.logger.debug(`cleanupGroups(currentLevel = ${currentLevel._self?.full_path}, collectedProjects = ${collectedProjects})`);
    const self = currentLevel._self;
    if (self === null) {
      this.logger.error('Error encountered during group cleanup process, ending early');
      return;
    }
    // collect and deduplicate project IDs
    const projects = [...Array.from(new Set([...currentLevel.projectTargets, ...collectedProjects]))];
    // build the user mapping to pass to cleanup
    let projectUsers: Record<string, EclipseUserAccess> = {};
    for (const pidx in projects) {
      projectUsers = { ...projectUsers, ...this.getUserList(this.eclipseProjectCache[projects[pidx]]) };
    }
    // clean up additional users
    await this.removeAdditionalUsers(projectUsers, self, ...projects);
    // for each of the children, pass the collected projects forward and process
    for (const cidx in currentLevel.children) {
      this.cleanupGroups(currentLevel.children[cidx], projects);
    }
  }

  /**
   * Removes users not tracked in the expectedUsers map from the passed group. Project IDs are used to look up bot user
   * account names as they are exempt from being removed as they are used for CI ops.
   *
   * @param expectedUsers map of usernames to their EclipseUser entry
   * @param group the Gitlab group that is being cleaned of extra users.
   * @param projectIDs list of project IDs that impact the given group
   * @returns a promise that completes once all additional users are removed or the check finishes
   */
  async removeAdditionalUsers(
    expectedUsers: Record<string, EclipseUserAccess>,
    group: GroupSchema,
    ...projectIDs: string[]
  ): Promise<void> {
    if (this.config.verbose) {
      this.logger.debug(
        `GitlabSync:removeAdditionalUsers(expectedUsers = ${JSON.stringify(expectedUsers)}, group = ${group.full_path
        }, projectIDs = ${projectIDs})`
      );
    }
    // get the current list of users for the group
    const members = await this.getGroupMembers(group);
    if (members === null || !(members instanceof Array)) {
      this.logger.warn(`Could not find any group members for group '${group.full_path}'. Skipping user removal check`);
      return;
    }
    // check that each of the users in the group match whats expected
    const expectedUsernames = Object.keys(expectedUsers);
    members?.forEach(async member => {
      // check access and ensure user isn't an owner
      this.logger.verbose(`Checking user '${member.username}' access to group '${group.name}'`);
      if (this.shouldRemoveUser(member, expectedUsers, projectIDs, expectedUsernames)) {
        if (this.config.dryRun) {
          this.logger.info(`Dryrun flag active, would have removed user '${member.username}' from group '${group.name}'`);
          return;
        }
        this.logger.info(`Removing user '${member.username}' from group '${group.name}'`);

        await this.gitlabWrapper.removeGroupmember(group.id, member.id)
          .catch(err => {
            if (this.config.verbose) {
              this.logger.error(`${err}`);
            }
            this.logger.warn(`Error while removing user '${member.username}' from group '${group.name}'`);
          });
      }
    });
  }

  /**
   * Checks for the following states:
   *
   * - User is outside the allowlisted users
   * - User is outside the expected user list
   * - The user has the wrong permissions set and isn't a project lead
   * - the user isn't a bot
   *
   * @param member the current group member being checked
   * @param expectedUsers the user access mapping for the current group
   * @param projectIDs projects associated with the current group
   * @param expectedUsernames the usernames from the users mapping, passed to save processing time
   * @returns true if all conditions in method description are met, otherwise false
   */
  shouldRemoveUser(
    member: MemberSchema,
    expectedUsers: Record<string, EclipseUserAccess>,
    projectIDs: string[],
    expectedUsernames: string[]
  ): boolean {
    return (
      allowlistedUsers.indexOf(member.username) === -1 &&
      (expectedUsernames.indexOf(member.username) === -1 ||
        (member.access_level !== expectedUsers[member.username]!.accessLevel &&
          expectedUsers[member.username]!.accessLevel !== maintainerPermissionsLevel)) &&
      !this.isBot(member.username, projectIDs)
    );
  }

  /**
   * Iterates over the projects cache and cleans out the users and keeps bots for build operations. Skips over projects
   * outside the scope of the designated root group to avoid over processing groups.
   */
  async cleanupProjects() {
    this.gitlabWrapper.getAllProjects().forEach(async p => {
      // don't process projects outside target namespace
      if (!this.withinNamespace(p.namespace.full_path)) {
        return;
      }

      // get the group of the project and clean it up
      const group = await this.getGroup(p.namespace.full_path);
      if (group !== null) {
        this.cleanUpProjectUsers(p, ...group.projectTargets);
      } else {
        this.logger.info(`Skipping processing of project '${p.name}'`);
      }
    });
  }

  /**
   * Removes any non-owner user that isn't a bot from projects. Membership is managed at the group level, not the direct
   * project level.
   *
   * @param project the Gitlab project to sanitize
   * @param projectIDs the Eclipse projects that impact the Gitlab project.
   */
  async cleanUpProjectUsers(project: ProjectSchema, ...projectIDs: string[]) {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:cleanUpProjectUsers(project = ${project.id})`);
    }
    const projectMembers = await this.gitlabWrapper.getAllProjectMembers(project.id);
    for (const idx in projectMembers) {
      const member = projectMembers[idx];
      this.logger.verbose(`Checking '${member.username}' for removal on project '${project.namespace.full_path}'(${member.access_level})`);
      // skip bot user or admin users
      if (this.isBot(member.username, projectIDs) || member.access_level === adminPermissionsLevel) {
        continue;
      }
      if (this.config.dryRun) {
        this.logger.debug(`Dryrun flag active, would have removed user '${member.username}' from project '${project.name}'(${project.id})`);
        continue;
      }
      this.logger.info(
        `Removing user '${member.username}' with permissions '${member.access_level}' from project ` + `'${project.name}'(${project.id})`
      );

      await this.gitlabWrapper.removeProjectMember(project.id, member.id)
        .catch(err => {
          if (this.config.verbose) {
            this.logger.error(`${err}`);
          }
          this.logger.error(`Error while removing user '${member.username}' from project '${project.name}'(${project.id})`);
        });
    }
  }

  /**
   * Fetches all private repos and determines whether or not they meet deletion criteria.
   * If they meet the criteria, checks for deletion issues and creates one if it does not already exist or
   * if the ones that do exist were created before the last activity date.
   */
  async managePrivateProjects(): Promise<void> {
    if (this.config.verbose) {
      this.logger.debug('GitlabSync:managePrivateProjects');
    }

    const privateProjects = await this.fetchPrivateProjects();
    if (privateProjects.length === 0) {
      this.logger.warn('Could not find any private projects');
      return;
    }

    const filteredProjects = privateProjects.filter(p => p.owner !== undefined || p.path_with_namespace.startsWith('eclipse/'));

    filteredProjects.forEach(async project => {
      if (this.isScheduledForDeletion(project.last_activity_at) &&
        (!this.projectHasDeletionIssue(project.id) || !this.issuesAfterLastActiveDate(project.id, project.last_activity_at))) {
        await this.createDeletionIssue(project.id);
      }
    });
  }

  /**
   * Fetches private projects from the gitlab api using axios. Returns an empty array if there was an error.
   * Using axios since the gitbreaker projects.all() does not return the ProjectExtendedSchema
   * required to obtain the 'visibility' property.
   * @returns ProjectExtendedShema array or null
   */
  async fetchPrivateProjects(): Promise<ProjectExtendedSchema[]> {
    try {
      if (this.config.verbose) {
        this.logger.debug('GitlabSync:fetchPrivateProjects');
      }

      let results: ProjectExtendedSchema[] = [];
      let hasMoreResults = true;
      let url = this.config.host.concat('/api/v4/projects?simple=no&visibility=private&per_page=100');

      while (hasMoreResults) {
        const response = await axios.get(url, {
          headers: { 'PRIVATE-TOKEN': this.accessToken }
        });

        results = results.concat(response.data as ProjectExtendedSchema[]);

        // Parse link headers and check for next URL
        const linkHeaders = parse(response.headers['link']);
        if (linkHeaders.next !== undefined) {
          url = linkHeaders.next.url;
        } else {
          hasMoreResults = false;
        }
      }

      return results;

    } catch (err) {
      this.logger.error(`Error while fetching private projects: ${err}`);
      return [];
    }
  }

  /**
   * Checks if the time since the last project activity is greater than 90 days and is therefore scheduled for deletion.
   * @param lastActivityAt the date of the latest project activity
   * @returns True if greater than or equal to 90 days
   */
  isScheduledForDeletion(lastActivityAt: string): boolean {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:isScheduledforDeletion(date=${lastActivityAt})`);
    }

    const timeSinceActivity = Date.now() - Date.parse(lastActivityAt);
    return timeSinceActivity >= repoExpiryAsMillis;
  }

  /**
   * Fetches the issues assigned to webmaster, then checks if any of their project ids match the given project id.
   * Returns true if any match exists
   * @param projectId the id of the project to check for issues
   * @returns True if deletion issue exists
   */
  projectHasDeletionIssue(projectId: number): boolean {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:projectHasDeletionIssue(projectId=${projectId})`);
    }

    return this.gitlabWrapper.getAllIssues().some(issue => (issue.project_id === projectId));
  }

  /**
  * Filters the issue cache for issues that match the given project id.
  * Checks these issues and determines whether or not any of them have been issued after the project's last activity date.
  * This flag is used to determine whether another deletion issue should be created.
  * @param projectId The project id used to filter issues
  * @param lastActivityAt The project's last activity date
  * @returns true if an issue was created after last activity date
  */
  issuesAfterLastActiveDate(projectId: number, lastActivityAt: string): boolean {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:issuedBeforeLastActiveDate(projectId=${projectId}, lastActivityAt=${lastActivityAt})`);
    }

    const deletionIssues = this.gitlabWrapper.getAllIssues().filter(issue => (issue.project_id === projectId));

    deletionIssues.forEach(issue => {
      if (Date.parse(issue.created_at as string) >= Date.parse(lastActivityAt)) {
        return true;
      }
    });

    return false;
  }

  /**
   * Creates a new issue under a given project using the project id. The issue acts as a flag for deletion.
   * @param projectId The project id for which to create the issue
   * @returns the new issue or null
   */
  async createDeletionIssue(projectId: number): Promise<IssueSchema> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:createDeletionIssue(projectId=${projectId})`);
    }

    const issueOptions = {
      title: 'Deletion of this project',
      description: 'This project has been inactive for 90 days and has been flagged for deletion.'
    };

    const newIssue = await this.gitlabWrapper.createIssue(projectId, issueOptions);
    if (newIssue !== null) {
      this.logger.info(`Created issue with id '${newIssue.id}' for project with id '${projectId}'`);
    }

    return newIssue;
  }

  /**
   * Ensures that the user exists within the group with the given access level (no more or less). If a user has too high
   * permissions, the membership is modified to have the given access instead.
   *
   * @param user the user that is being given permissions
   * @param group group that the user should be added to
   * @param perms the permission set to give the user
   * @returns the membership information for the user wrt to this Gitlab group.
   */
  async addUserToGroup(user: UserSchema, group: GroupSchema, perms: AccessLevel): Promise<MemberSchema | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:addUserToGroup(user = ${user?.username}, group = ${group?.full_path}, perms = ${perms})`);
    }
    // get the members for the current group
    const members = await this.getGroupMembers(group);
    if (members === null) {
      this.logger.warn(`Could not find any references to group with ID ${group.id}`);
      return null;
    }

    // check if user is already present
    let existingMember = members.find(member => member.username === user.username);
    if (existingMember !== undefined) {
      this.logger.verbose(`User '${user.username}' is already a member of ${group.name}`);
      if (existingMember.access_level !== perms) {
        // skip if dryrun
        if (this.config.dryRun) {
          this.logger.info(`Dryrun flag active, would have updated user '${existingMember.username}' in group '${group.name}'`);
          return null;
        }

        // modify user
        this.logger.info(`Fixing permission level for user '${user.username}' in group '${group.name}'`);

        const updatedMember = await this.gitlabWrapper.editGroupMember(group.id, user.id, perms);
        if (updatedMember === null) {
          this.logger.warn(`Error while fixing permission level for user '${user.username}' in group '${group.name}'`);
          return null;
        }

        existingMember = updatedMember;
      }

      // return a copy of the updated user
      return existingMember;
    }

    // check if dry run before updating
    if (this.config.dryRun) {
      this.logger.info(
        `Dryrun flag active, would have added user '${user.username}' to group '${group.name}' with access level '${perms}'`
      );
      return null;
    }

    this.logger.info(`Adding '${user.username}' to '${group.name}' group`);

    // add member to group, track, and return a copy
    const newMember = await this.gitlabWrapper.addGroupMember(group.id, user.id, perms);
    if (newMember === null) {
      this.logger.warn(`Error while adding '${user.username}' to '${group.name}' group`);
      return null;
    }

    return newMember;
  }

  /**
   * Retrieves a Gitlab user object for the given Eclipse user given their username and access URL. If the
   * user does not yet exist, a new user is created, cached, and returned for use.
   *
   * @param uname the Eclipse username of user to retrieve from Gitlab
   * @param url the Eclipse user access URL
   * @returns the gitlab user, or null if it can't be found or created.
   */
  async getUser(uname: string, url: string): Promise<UserSchema | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:getUser(uname = ${uname}, url = ${url})`);
    }
    if (url === undefined || url === '') {
      this.logger.error(`Cannot fetch user information for user '${uname}' with no set URL`);
      return null;
    }

    let u = this.gitlabWrapper.getNamedUser(uname);
    if (u === undefined) {
      if (this.config.dryRun) {
        this.logger.info(`Dryrun is enabled. Would have created user ${uname} but was skipped`);
        return null;
      }

      // retrieve user data
      const data = await this.eApi.eclipseUser(uname);
      if (data === null) {
        this.logger.error(`Cannot create linked user account for '${uname}', no external data found`);
        return null;
      }
      this.logger.verbose(`Creating new user with name '${uname}'`);
      const opts = {
        username: uname,
        password: v4(),
        force_random_password: true,
        name: `${data.first_name} ${data.last_name}`,
        email: data.mail,
        extern_uid: data.uid,
        provider: this.config.provider,
        skip_confirmation: true,
      };
      // check if dry run before creating new user
      if (this.config.dryRun) {
        this.logger.info(`Dryrun flag active, would have created new user '${uname}' with options ${JSON.stringify(opts)}`);
        return null;
      }

      // if verbose, display information being used to generate user
      if (this.config.verbose) {
        // copy the object and redact the password for security
        const optLog = JSON.parse(JSON.stringify(opts));
        optLog.password = 'redacted';
        this.logger.debug(`Creating user with options: ${JSON.stringify(optLog)}`);
      }

      u = await this.gitlabWrapper.createUser(opts);

      if (u === null) {
        this.logger.warn(`Error while creating user '${uname}'`);
        return null;
      }
    }
    return u;
  }

  /**
   * Retrieves the list of direct members for a given group, ignoring inherited users.
   *
   * @param group the Gitlab group to retrieve members for
   * @returns a list of Gitlab group members for the given group, or null if there is an error while fetching.
   */
  async getGroupMembers(group: GroupSchema): Promise<MemberSchema[] | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:getGroupMembers(group = ${group?.full_path})`);
    }

    const members = await this.gitlabWrapper.getGroupMembers(group.id);
    if (members === null) {
      this.logger.warn(`Unable to find group members for group with ID '${group.id}'`);
      return null;
    }

    return [...members];
  }

  /** HELPERS */

  /**
  * Adds a group to the nested group cache, using the groups full_path property to discover how to insert the
  * entry into the nested cache. Any cache nodes that do not exist yet will be created as the group is inserted.
  *
  * @param g the Gitlab group that is being inserted into the group cache.
  * @returns the group cache entry for the cached gitlab group
  */
  addGroup(g: GroupSchema): GroupCache {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:addGroup(g = ${g.id})`);
    }
    const namespace = g.full_path.toLowerCase();
    // split into group namespace paths (eclipse/sample/group.path into ['eclipse','sample','group.path'])
    const namespaceParts = namespace.split('/');
    return this.tunnelAndInsert(namespaceParts, g, this.nestedGroupCache);
  }

  /**
  * Retrieves the group for the given namespace path. This namespace path should be formatted such that each group path is separated
  * by a slash, eg. eclipse/sample/group. This will be split and used to iterate through the nested cache, returning the group once
  * each namespace path part is used.
  *
  * @param namespace the full path of the group namespace to retrieve.
  * @returns the group cache for the group indicated by the namespace string, or null if there is no matching group.
  */
  async getGroup(namespace: string, rootGroup: string = this.config.rootGroup): Promise<GroupCache | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:getCachedGroup(${namespace})`);
    }

    const normalizedNamespace = namespace.toLowerCase();
    if (!this.withinNamespace(normalizedNamespace, rootGroup)) {
      this.logger.info(`Returning null for ${namespace} as it is outside of the root group ${rootGroup}`);
      return null;
    }

    return this.tunnelAndRetrieve(normalizedNamespace.split('/'), this.nestedGroupCache);
  }


  /**
  * Generate the nested group cache using the raw Gitlab group definitions.
  * @param rawGroups
  */
  generateGroupsCache(rawGroups: GroupSchema[]): void {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:generateGroupsCache(projects = count->${rawGroups.length})`);
    }
    // create initial cache container
    this.nestedGroupCache = {
      _self: null,
      projectTargets: [],
      children: {},
    };

    // iterate through groups and insert into the nested cache
    for (const element of rawGroups) {
      this.addGroup(element);
    }
  }

  /**
   * HELPERS
   */

  /**
  * Recursive function for inserting groups into the nested group cache. Will tunnel through cache, creating
  * entries as necessary before inserting the group at the nesting level representing the final group in the
  * full path of the group.
  *
  * @param namespaceParts the parts of the full_path left to process for group nesting
  * @param g  the group to be inserted into the group cache.
  * @param parent the parent level for the current level of insertion
  * @returns the group cache entry for the group once inserted.
  */
  tunnelAndInsert(namespaceParts: string[], g: GroupSchema, parent: GroupCache): GroupCache {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:tunnelAndInsert(namespaceParts = '${namespaceParts}', g = ${g.id})`);
    }

    // get the next level cache if it exists, creating it if it doesn't
    let child = parent.children[namespaceParts[0]];
    if (child === undefined) {
      child = {
        _self: null,
        projectTargets: [],
        children: {},
      };
      parent.children[namespaceParts[0]] = child;
    }
    // check if we should continue tunneling or insert and finish processing
    if (namespaceParts.length > 1) {
      return this.tunnelAndInsert(namespaceParts.slice(1, namespaceParts.length), g, child);
    }
    child._self = g;
    return child;
  }

  /**
  * Recursive access to the nested group cache. Retrieves the group described by the namespace parts and returns
  * it, returning null if it can't be found.
  *
  * @param namespaceParts the full path for a group namespace split into parts
  * @param parent the parent to search through for the next part of the recursive call.
  * @returns The group cache for the designated group, or null if it can't be found.
  */
  async tunnelAndRetrieve(namespaceParts: string[], parent: GroupCache): Promise<GroupCache | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:tunnelAndRetrieve(namespaceParts = '${namespaceParts}')`);
    }

    let child = parent.children[namespaceParts[0]];
    if (child === undefined) {
      // attempt to create the new group
      const newGroup = await this.createMissingGroup(namespaceParts[0], parent._self);
      if (newGroup === null) {
        this.logger.warn(`Could not create missing group with name '${namespaceParts[0]}' in group with path '${parent._self.full_path}'`);
        return null;
      }

      // insert the new child group into the cache and continue
      child = this.tunnelAndInsert(newGroup.full_path.split('/'), newGroup, this.getRootGroup());
    }

    // check if we should continue tunneling or insert and finish processing
    if (namespaceParts.length > 1) {
      return this.tunnelAndRetrieve(namespaceParts.slice(1, namespaceParts.length), child);
    }
    return child;
  }

  /**
  * @returns the root group cache for the current sync operation if it exists. If missing, the script ends processing.
  */
  getRootGroup(endProcessing = true): GroupCache {
    const rootGroupCache = this.nestedGroupCache.children[this.config.rootGroup];
    if (rootGroupCache === undefined) {
      if (endProcessing) {
        this.logger.error(`Could not find root group '${this.config.rootGroup}' for group caching, exiting`);
        process.exit(1);
      }
      throw new Error(`Could not find root group '${this.config.rootGroup}' for root group fetch`);
    }
    return rootGroupCache;
  }

  /**
  * Used to create missing groups in the Gitlab instance. Does not insert into the nest cache as this should only be
  * called from said cache. This method does not support creating root level groups.
  *
  * @param name the name of the group to create
  * @param parent the group that this group belongs to
  * @returns the new group schema once the call finishes
  */
  async createMissingGroup(name: string, parent: GroupSchema): Promise<GroupSchema | null> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:createMissingGroup(name = ${name}, parent = ${parent.id})`);
    }

    // default options for creating new group
    const opts = {
      project_creation_level: 'maintainer',
      visibility: 'public',
      request_access_enabled: false,
      parent_id: parent.id,
    };

    this.logger.info(`Creating missing group '${name}' in namespace '${parent.full_path} (${parent.id})'`);
    return this.gitlabWrapper.createGroup(name, name, opts);
  }

  /**
   * Adapt interest groups into synthetic projects. These adapted projects can be used in downstream calls as if they
   * are projects for sync operations.
   *
   * @param interestGroups the groups to be adapted
   * @returns the list of adapted projects
   */
  adaptInterestGroupsToProject(interestGroups: InterestGroup[]): EclipseProject[] {
    return interestGroups
      .filter(ig => ig.state.localeCompare('active') === 0)
      .map(ig => {
        return {
          project_id: ig.foundationdb_project_id,
          name: ig.title,
          logo: ig.logo,
          state: ig.state,
          summary: ig.description.summary,
          short_project_id: ig.foundationdb_project_id.substring(ig.foundationdb_project_id.lastIndexOf('.') + 1),
          gitlab: ig.gitlab,
          project_leads: ig.leads,
          committers: ig.participants,
          contributors: [],
          gerrit_repos: [],
          github_repos: [],
          gitlab_repos: [],
          website_repo: [],
          releases: [],
          spec_project_working_group: [],
          working_groups: [],
          tags: [],
          top_level_project: '',
          url: '',
          website_url: '',
        } as EclipseProject;
      });
  }

  /**
   * Gets list of users with access permissions for the given Eclipse project.
   *
   * @param project the Eclipse project to parse user entries for
   * @returns the mapping of users to access permissions and entity access URL.
   */
  getUserList(project: EclipseProject): Record<string, EclipseUserAccess> {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:getUserList(project = ${project.project_id})`);
    }
    const l: Record<string, EclipseUserAccess> = {};
    // add the contributors with reporter access
    project.contributors.forEach(v => {
      l[v.username] = {
        url: v.url,
        accessLevel: 20,
      };
    });
    // add the committers with developer access
    project.committers.forEach(v => {
      l[v.username] = {
        url: v.url,
        accessLevel: 30,
      };
    });
    // add the project leads not yet tracked with reporter access
    project.project_leads.forEach(v => {
      l[v.username] = {
        url: v.url,
        accessLevel: 40,
      };
    });
    // add the bots with developer access
    const botList = this.botsCache[project.project_id];
    if (botList !== undefined && botList.length === 0) {
      botList.forEach(v => {
        l[v] = {
          url: '',
          accessLevel: 30,
        };
      });
    }
    return l;
  }

  /**
   * Sanitizes and normalizes strings for use in creating/accessing groups.
   *
   * @param pid the project ID to normalize
   * @returns normalized group name for value.
   */
  sanitizeGroupName(pid: string): string {
    if (this.config.verbose) {
      this.logger.debug(`GitlabSync:sanitizeGroupName(pid = ${pid})`);
    }
    if (pid !== undefined) {
      return pid.toLowerCase().replace(/[^\s\da-zA-Z-.]/g, '-');
    }
    return '';
  }

  /**
   * Checks whether a user is a bot for the given projects.
   *
   * @param uname potential bot username
   * @param projectIDs the projects that the user could be a bot for.
   * @returns true if the user is a designated bot for the projects, otherwise false.
   */
  isBot(uname: string, projectIDs: string[]): boolean {
    return projectIDs.some(v => this.botsCache[v] !== undefined && this.botsCache[v]!.indexOf(uname) !== -1);
  }

  /**
   * Check to ensure that a given namespace is within the target group namespace.
   *
   * @param namespace the namespace to check
   * @returns true if the namespace is under the configured root group, false otherwise.
   */
  withinNamespace(namespace: string, rootGroup: string = this.config.rootGroup): boolean {
    return namespace.startsWith(rootGroup + '/');
  }
}
